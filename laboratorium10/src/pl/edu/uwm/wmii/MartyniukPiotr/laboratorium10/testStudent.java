package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium10;
import pl.edu.uwm.wmii.MartyniukPiotr.laboratorium10.pl.imiajd.Martyniuk.Student;
import java.util.ArrayList;
import java.util.Collections;

public class testStudent {
    public static void main(String[] args){
        ArrayList<Student> grupa = new ArrayList<Student>();
        grupa.add(new Student("Maksymow", 2000, 3, 19, 2.0));
        grupa.add(new Student("Minimow", 2000, 3, 17, 2.3));
        grupa.add(new Student("Ekstremow", 2000, 8, 21, 3.0));
        grupa.add(new Student("Martynow", 2000, 8, 21, 3.0));
        grupa.add(new Student("Kekow", 2000, 5, 13, 5.0));

        Collections.sort(grupa);

        for(int i = 0; i<5; i++)
            System.out.println(grupa.get(i).toString());
    }
}

