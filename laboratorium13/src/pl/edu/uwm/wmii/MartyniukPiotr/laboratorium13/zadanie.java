package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium13;

public class zadanie implements Comparable<zadanie>{
    int priorytet;
    String opis;

    public zadanie(int priorytet, String opis){
        this.priorytet = priorytet;
        this.opis = opis;
    }

    @Override
    public int compareTo(zadanie zad2){
        if(this.priorytet < zad2.priorytet)
            return 1;
        if(this.priorytet > zad2.priorytet)
            return -1;
        return 0;
    }

    public String getOpis(){
        return opis;
    }

    public int getPriorytet(){
        return priorytet;
    }
}
