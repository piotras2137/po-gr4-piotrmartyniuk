package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium10;

import pl.edu.uwm.wmii.MartyniukPiotr.laboratorium10.pl.imiajd.Martyniuk.Osoba;

import java.util.ArrayList;
import java.util.Collections;

public class testOsoba {
    public static void main(String[] args){
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();
        grupa.add(new Osoba("Maksymow", 2000, 3, 19));
        grupa.add(new Osoba("Minimow", 2000, 3, 17));
        grupa.add(new Osoba("Martyniuk", 2000, 8, 10));
        grupa.add(new Osoba("Olszewski", 2000, 8, 18));
        grupa.add(new Osoba("Adrianow", 2000, 5, 13));

        Collections.sort(grupa);

        for(int i = 0; i<5; i++)
            System.out.println(grupa.get(i).toString());
    }
}
