package pl.edu.uwm.wmii.MartyniukPiotr.Test2;
import java.util.LinkedList;


public class Redukcja {
    public static <T> void redukuj(LinkedList<T> ksiazki, int n) {
        for (int i = 0; i < ksiazki.size(); i += (n - 1)) {
            ksiazki.remove(i);
        }
        System.out.println(ksiazki);
    }
}
