package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium11;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Integer[] tab1 = {Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3)};
        Integer[] tab2 = {Integer.valueOf(3), Integer.valueOf(2), Integer.valueOf(1)};
        LocalDate[] tab3 = {LocalDate.of(1950, 5, 12), LocalDate.of(1970,1,1)};
        LocalDate[] tab4 = {LocalDate.of(1970, 5, 12), LocalDate.of(1950,1,1)};

        System.out.println(ArrayUtil.isSorted(tab1));
        System.out.println(ArrayUtil.isSorted(tab2));
        System.out.println(ArrayUtil.isSorted(tab3));
        System.out.println(ArrayUtil.isSorted(tab4));

        System.out.println(ArrayUtil.binSearch(tab1, tab1[1]));
        System.out.println(ArrayUtil.binSearch(tab3, tab3[1]));

        ///ArrayUtil.selectionSort(tab2);
        ///ArrayUtil.selectionSort(tab4);

        ArrayUtil.mergeSort(tab2, 0, 2);
        ArrayUtil.mergeSort(tab4, 0, 1);

        for(int i = 0; i<tab2.length; i++)
            System.out.println(tab2[i]);
        for(int i = 0; i<tab4.length; i++)
            System.out.println(tab4[i]);
    }
}
