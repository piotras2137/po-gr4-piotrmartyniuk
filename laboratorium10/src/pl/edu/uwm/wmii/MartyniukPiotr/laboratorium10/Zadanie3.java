package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium10;


import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

public class Zadanie3 {
    public static void main(String[] args){
        try {
            File file = new File(args[0]);
            Scanner scanner = new Scanner(file);

            ArrayList<String> linijki = new ArrayList<String>();
            String reader;
            while (scanner.hasNextLine()) {
                reader = scanner.nextLine();
                linijki.add(reader);
            }

            Collections.sort(linijki);
            for(int i = 0; i<linijki.size(); i++)
                System.out.println(linijki.get(i));
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
