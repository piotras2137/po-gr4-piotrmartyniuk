package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium13;

import java.util.TreeMap;
import java.util.Scanner;

public class zad2 {

    public static void program(){
        Scanner scanner = new Scanner(System.in);
        TreeMap<String,String> studenci = new TreeMap<>();
        String polecenie = scanner.next();
        String nazwisko;
        String ocena;
        while(polecenie.equals("zakończ") == false){
            if(polecenie.equals("dodaj")){
                nazwisko = scanner.next();
                ocena = scanner.next();
                studenci.put(nazwisko, ocena);
            }
            if(polecenie.equals("usuń")){
                nazwisko = scanner.next();
                studenci.remove(nazwisko);
            }
            if(polecenie.equals("wypisz")){
                for (String i : studenci.keySet()) {
                    System.out.println(i + ": " + studenci.get(i));
                }
            }
            polecenie = scanner.next();
        }
    }
}
