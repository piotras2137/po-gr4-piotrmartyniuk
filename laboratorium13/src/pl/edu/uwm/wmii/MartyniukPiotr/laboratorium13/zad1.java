package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium13;

import java.util.Scanner;
import java.util.PriorityQueue;

public class zad1 {

    public static void program() {
        Scanner scanner = new Scanner(System.in);
        PriorityQueue<zadanie> zadania = new PriorityQueue<>();
        String polecenie = scanner.next();
        int priorytet;
        String opis;
        while(polecenie.equals("zakończ") == false){
            if (polecenie.equals("dodaj")) {
                priorytet = scanner.nextInt();
                opis = scanner.nextLine();
                zadania.add(new zadanie(priorytet, opis));
            }
            if (polecenie.equals("następne") && zadania.isEmpty() == false)
                zadania.poll();
            polecenie = scanner.next();
        }
    }
}
