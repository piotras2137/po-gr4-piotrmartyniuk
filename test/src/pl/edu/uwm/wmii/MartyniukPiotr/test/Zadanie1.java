package pl.edu.uwm.wmii.MartyniukPiotr.test;
import java.util.Scanner;

public class Zadanie1 {
    public static void test(int n)
    {
        Scanner sc=new Scanner(System.in);
        int[] tabela=new int[n];
        int minus5=0, wiekszeod5=0, mniejszeodminus5=0;

        for(int i=0; i<n;i++)
        {
            tabela[i]=sc.nextInt();
        }

        for(int i=0; i<n; i++)
        {
            if (tabela[i]==-5)
            {
                minus5++;
            }
            if(tabela[i]<-5)
            {
                mniejszeodminus5++;
            }
            if(tabela[i]>5)
            {
                wiekszeod5++;
            }
        }
        System.out.print("wieksze od 5 ");
        System.out.println(wiekszeod5);
        System.out.print("mniejsze od -5 ");
        System.out.println(mniejszeodminus5);
        System.out.print("równe -5 ");
        System.out.println(minus5);
    }
}
