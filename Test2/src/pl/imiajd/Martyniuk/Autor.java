package pl.imiajd.Martyniuk;

import java.util.ArrayList;


public class Autor implements Cloneable, Comparable<Autor> {

    private String nazwa;
    private String email;
    private char plec;


    public Autor(String nazwa, String email, char plec) {
        this.nazwa = nazwa;
        this.email = email;
        this.plec = plec;
    }

    public String getNazwa()
    {
        return this.nazwa;
    }

    public String getEmail() {
        return this.email;
    }

    public char getPlec() {
        return this.plec;
    }

    public void setNazwa (String nazwa){
        this.nazwa = nazwa;
    }

    public void setEmail (String email){
        this.email = email;
    }

    public void setPlec (char plec){
        this.plec = plec;
    }

    @Override
    public int compareTo(Autor aut) {
        int compare_nazwa = this.nazwa.compareTo(aut.nazwa);
        if(compare_nazwa==0){
            return this.plec-aut.plec;
        }
        return compare_nazwa;
    }

    public Autor clone() {
        try {
            return (Autor) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public String toString()
    {
        String plec=new String("");
        if (this.plec=='M'){
            plec+=" mezczyzna";
        }
        else {
            plec+=" kobieta";
        }
        return this.getClass().getSimpleName()+" Autor : [Nazwisko: "+this.nazwa+" Email: "+this.email+" Plec"+plec+"]\n";
    }


}
