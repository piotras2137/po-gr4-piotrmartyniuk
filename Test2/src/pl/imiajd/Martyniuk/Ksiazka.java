package pl.imiajd.Martyniuk;
import java.time.LocalDate;
public class Ksiazka implements Comparable<Ksiazka>, Cloneable
{
    private String tytul;
    private Autor autor;
    private double cena;


    public Ksiazka(String tytul, Autor autor, double cena) {
        this.autor = autor;
        this.tytul = tytul;
        this.cena = cena;
    }

    public int compareTo(Ksiazka o) {
        int compare_nazwa = this.autor.compareTo(o.autor);
        if(compare_nazwa==0){
            int compare_tytul = this.tytul.compareTo(o.tytul);
            if(compare_tytul==0)
                return (int) (this.cena-o.cena);
        }
        return compare_nazwa;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Autor: "+this.autor.toString()+" Tytuł: "+this.tytul+" Cena "+this.cena+"\n";
    }
}

