package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium11;

import java.util.ArrayList;

public class ArrayUtil {
    public static <T extends Comparable<? super T>> boolean isSorted(T tab[]){
        boolean keks = true;

        for(int i = 1; i<tab.length; i++)
            if(tab[i].compareTo(tab[i-1]) < 0)
                keks = false;

        return keks;
    }

    public static <T extends Comparable<? super T>> int binSearch(T tab[], T element){
        int a = 0;
        int b = tab.length-1;
        int curindeks;
        int wynik;

        while(a<=b){
            curindeks = (a+b)/2;
            wynik = tab[curindeks].compareTo(element);
            if(wynik > 0)
                b = curindeks-1;
            if(wynik < 0)
                a = curindeks+1;
            if(wynik == 0)
                return curindeks;
        }

        return -1;
    }

    public static <T extends Comparable<? super T>> void selectionSort(T tab[]) {
        int cur_indeks = 0;
        T min;
        int min_indeks;
        T temp;

        for (int i = 0; i < tab.length; i++) {
            min = tab[cur_indeks];
            min_indeks = cur_indeks;
            for (int j = cur_indeks + 1; j < tab.length; j++) {
                if (tab[j].compareTo(min) < 0) {
                    min = tab[j];
                    min_indeks = j;
                }
            }
            temp = tab[cur_indeks];
            tab[cur_indeks] = min;
            tab[min_indeks] = temp;
            cur_indeks++;
        }
    }

    public static <T extends Comparable<? super T>> void mergeSort(T tab[], int p, int r){
        if(p < r) {
            int q = p+r/2;
            mergeSort(tab, p, q);
            mergeSort(tab, q+1, r);
            merge(tab, p, q, r);
        }
    }

    private static <T extends Comparable<? super T>> void merge(T tab[], int p, int r, int q){
        ArrayList<T> help_tab = new ArrayList<T>();

        for(int i = p; i<=q; i++)
            help_tab.add(tab[i]);

        int i = p;
        int j = r+1;
        int k = p;
        while(i<=r && j<=q){
            if(help_tab.get(i).compareTo(help_tab.get(j)) < 0)
                tab[k++] = help_tab.get(i++);
            else
                tab[k++] = help_tab.get(j++);
        }
        while(i<=r)
            tab[k++] = help_tab.get(i++);
    }
}
