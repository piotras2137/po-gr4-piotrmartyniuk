package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.Map;

public class zad4 {
    public static void program(){
        try {
            TreeMap<Integer, HashSet<String>> mapa = new TreeMap<>();
            Integer code;
            HashSet<String> zbior;
            File file = new File("plik.txt");
            String reader;
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                reader = scanner.next();
                code = reader.hashCode();
                if(mapa.containsKey(code))
                    mapa.get(code).add(reader);
                zbior = new HashSet<>();
                zbior.add(reader);
                mapa.put(code, zbior);
            }

            for(Map.Entry<Integer, HashSet<String>> entry : mapa.entrySet())
                if(entry.getValue().size() > 1)
                    System.out.printf("Kod mieszający powtarza się dla %d", entry.getKey());

        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();

        }
    }
}
