package pl.edu.uwm.wmii.MartyniukPiotr.Test2;
import pl.imiajd.Martyniuk.*;

import java.util.*;

public class Main {
    public static void main(String args[]) {
        System.out.println("Zadanie 1 \n");////zadanie 1

        ArrayList<Autor> autorzy = new ArrayList<>();
        autorzy.add(0, new Autor("Tolkien","hobbit@gmail.com",'M'));
        autorzy.add(1, new Autor("Tolkien","bilbo@gmail.com",'K'));
        autorzy.add(2, new Autor("Mirek Coder","compiler1968@gmail.com",'M'));
        autorzy.add(3, new Autor("Asia Podrywanie","asiakasia@gmail.com",'K'));

        System.out.println("\nPrzed Sortowaniem :\n");

        for(Autor x:autorzy){
            System.out.print(x.toString());
        }

        Collections.sort(autorzy);

        System.out.println("\n\nPo Sortowaniu :\n");

        for(Autor x:autorzy){
            System.out.print(x.toString());
        }

        System.out.println("\n\n");

        ArrayList<Ksiazka> Listaksiazek = new ArrayList<>();
        Listaksiazek.add(0, new Ksiazka("Hobbit",autorzy.get(0),21.37));
        Listaksiazek.add(1, new Ksiazka("Hobbit2",autorzy.get(0),44.50));
        Listaksiazek.add(2, new Ksiazka("Barka",autorzy.get(1),16.50));
        Listaksiazek.add(3, new Ksiazka("C poradnik",autorzy.get(3),23.90));

        System.out.println("lita ksiazek  :");
        for(Ksiazka x:Listaksiazek){
            System.out.print(x.toString());
        }

        System.out.println("\n\nZadanie 2\n\n");///////zadanie 2

        LinkedList<Ksiazka> booklist = new LinkedList<>();
        booklist.add(0, new Ksiazka("Hobbit",autorzy.get(0),21.37));
        booklist.add(1, new Ksiazka("Hobbit2",autorzy.get(0),44.50));
        booklist.add(2, new Ksiazka("Barka",autorzy.get(1),16.50));
        booklist.add(3, new Ksiazka("C poradnik",autorzy.get(3),23.90));

        System.out.println("lista ksiazek do skasowania: \n");
        for(Ksiazka x:booklist){
            System.out.print(x.toString());
        }
        Redukcja.redukuj(booklist,3);

        System.out.println("\nta sama lista po usunięciu co ntej ksiazki: \n");
        for(Ksiazka x:booklist){
            System.out.print(x.toString());
        }

   }
}

