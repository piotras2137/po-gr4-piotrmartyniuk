package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium13;

import java.util.TreeMap;
import java.util.Scanner;
import java.util.ArrayList;

public class zad3 {
    public static void program() {
        TreeMap<student, String> studenciak = new TreeMap<>();
        ArrayList<student> studenci = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        String polecenie = scanner.next();
        String imie;
        String nazwisko;
        String ocena;

        while(!polecenie.equals("zakończ")){
            if(polecenie.equals("dodaj")){
                imie = scanner.next();
                nazwisko = scanner.next();
                ocena = scanner.next();

                student temp = new student(imie, nazwisko);
                studenci.add(temp);
                studenciak.put(temp, ocena);
            }
            if(polecenie.equals("usuń")){
                int id = scanner.nextInt();
                student temp;
                for(int i = 0; i<studenci.size(); i++) {
                    if (studenci.get(i).getStudent_id() == id) {
                        temp = studenci.get(i);
                        studenciak.remove(temp);
                        studenci.remove(temp);
                    }
                }
            }
            if(polecenie.equals("wypisz")){
                studenci.sort(student::compareTo);
                for(int i = 0; i<studenci.size(); i++){
                    System.out.printf("%s %s %s\n", studenci.get(i).getImie(), studenci.get(i).getNazwisko(), studenciak.get(studenci.get(i)));
                }
            }
            polecenie = scanner.next();
        }
    }
}
