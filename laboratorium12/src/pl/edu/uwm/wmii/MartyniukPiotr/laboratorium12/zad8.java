package pl.edu.uwm.wmii.MartyniukPiotr.laboratorium12;

import java.util.ArrayList;
import java.util.Arrays;

public class zad8 {
    public static <T extends Iterable<T>> void print(T tab) {
        if(tab.iterator().hasNext())
            System.out.print(tab.iterator().next());
        while(tab.iterator().hasNext()){
            System.out.print(',');
            System.out.print(tab.iterator().next());
        }
    }
}
